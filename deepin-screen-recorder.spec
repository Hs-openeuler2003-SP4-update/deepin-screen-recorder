Name:           deepin-screen-recorder
Version:        5.11.15
Release:        1
Summary:        Deepin Screen Recorder
License:        GPLv3+
URL:            https://github.com/linuxdeepin/deepin-screen-recorder
Source0:        https://github.com/linuxdeepin/%{name}/archive/%{version}/%{name}-%{version}.tar.gz
Patch0:         0001-delete-proc-readproc.h.patch

BuildRequires:  qt5-devel
BuildRequires:  dtkcore-devel
BuildRequires:  qt5-linguist
BuildRequires:  dtkwidget-devel
BuildRequires:  procps-ng-devel
BuildRequires:  qt5-qtmultimedia-devel
BuildRequires:  pkgconfig(Qt5Core)
BuildRequires:  pkgconfig(Qt5DBus)
BuildRequires:  pkgconfig(Qt5Gui)
BuildRequires:  pkgconfig(Qt5Network)
BuildRequires:  pkgconfig(Qt5Widgets)
BuildRequires:  pkgconfig(Qt5X11Extras)
BuildRequires:  pkgconfig(dframeworkdbus)
BuildRequires:  pkgconfig(x11)
BuildRequires:  pkgconfig(xext)
BuildRequires:  pkgconfig(xtst)
BuildRequires:  pkgconfig(xcb)
BuildRequires:  pkgconfig(xcb-util)
BuildRequires:  pkgconfig(xcursor)
BuildRequires:  desktop-file-utils
BuildRequires:  libappstream-glib
BuildRequires:  gcc-c++
BuildRequires:  gcc
BuildRequires:  dde-dock-devel
BuildRequires:  qt5-qtbase-private-devel
BuildRequires:  libusbx-devel
BuildRequires:  gstreamer1-plugins-base-devel
BuildRequires:  v4l-utils-devel
BuildRequires:  systemd-devel
BuildRequires:  ffmpeg-devel
BuildRequires:  portaudio-devel
BuildRequires:  ffmpegthumbnailer-devel
BuildRequires:  libimagevisualresult-devel
Requires:       byzanz
Requires:       ffmpeg
Requires:       hicolor-icon-theme
Requires:       dbus
Requires:       gstreamer1-plugins-good
Requires:       deepin-turbo
Recommends:     gstreamer1-plugins-ugly-free


%description
%{summary}.

%prep
%autosetup -p1

sed -i 's|/lib|/%{_lib}|' src/dde-dock-plugins/recordtime/recordtime.pro
sed -i 's|/lib|/%{_lib}|' src/dde-dock-plugins/shotstart/shotstart.pro

%build
export PATH=%{_qt5_bindir}:$PATH
CFLAGS="${CFLAGS:-%optflags} -I/usr/include/ffmpeg"
CXXFLAGS="${CXXFLAGS:-%optflags} -I/usr/include/ffmpeg"
%qmake_qt5 PREFIX=%{_prefix}
%make_build

%install
%make_install INSTALL_ROOT=%{buildroot}


%files
%doc README.md
%license LICENSE
%{_bindir}/%{name}
%{_bindir}/deepin-pin-screenshots
%{_datadir}/%{name}/
%{_datadir}/deepin-manual/manual-assets/application/%{name}/

%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/*/apps/%{name}.svg
%{_datadir}/icons/hicolor/scalable/apps/deepin-screenshot.svg
%{_datadir}/dbus-1/services/com.deepin.ScreenRecorder.service
%{_datadir}/dbus-1/services/com.deepin.PinScreenShots.service
%{_libdir}/dde-dock/plugins/libdeepin-screen-recorder-plugin.so
%{_libdir}/dde-dock/plugins/libshot-start-plugin.so
%{_datadir}/dbus-1/services/com.deepin.Screenshot.service
%{_datadir}/glib-2.0/schemas/com.deepin.dde.dock.module.shot-start-plugin.gschema.xml

%changelog
* Tue Jul 25 2023 leeffo <liweiganga@uniontech.com> - 5.11.15-1
- upgrade to version 5.11.15

* Wed Nov 16 2022 liweiganga <liweiganga@uniontech.com> - 5.11.12-1
- update to 5.11.12

* Wed Aug 03 2022 liweiganga <liweiganga@uniontech.com> - 5.9.13-2
- modify installation dependency type

* Tue Jul 19 2022 loong_C <loong_c@yeah.net> - 5.9.13-1
- update to 5.9.13

* Tue Feb 08 2022 liweigang <liweiganga@uniontech.com> - 5.8.0.52-3
- fix build error

* Wed Aug 11 2021 weidong <weidong@uniontech.com> - 5.8.0.52-2
- Fix build failure with gcc-10.

* Sat May 08 2021 weidong <weidong@uniontech.com> - 5.8.0.52-1
- Initial package. 
